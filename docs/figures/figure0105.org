
#+name: tree-recur-table
#+attr_html: display="none"
| a | fib 5 |
| b | fib 4 |
| c | fib 3 |
| d | fib 3 |
| e | fib 2 |
| f | fib 2 |
| g | fib 1 |
| h | fib 2 |
| i | fib 1 |
| j | fib 1 |
| k | fib 0 |
| l | fib 1 |
| m | fib 0 |
| n | fib 1 |
| o | fib 0 |
| p | 1     |
| q | 1     |
| r | 0     |
| s | 1     |
| t | 0     |
| u | 1     |
| v | 0     |

#+name: tree-recur-dot
#+begin_src emacs-lisp :var table=tree-recur-table :results output :exports none
  (defun node (n)
    (gv-node n table))

  (defun nodes (ns)
    (gv-nodes ns table))

  (gv-define-monospace-nodes table)

  (gv-edge-list (node 0)
                (nodes '(1 2)))

  (gv-edge-list (node 1)
                (nodes '(3 4)))

  (gv-edge-list (node 2)
                (nodes '(5 6)))

  (gv-edge-list (node 3)
                (nodes '(7 8)))

  (gv-edge-list (node 4)
                (nodes '(9 10)))

  (gv-edge-list (node 5)
                (nodes '(11 12)))

  (gv-edge (node 6)
           (node 15))
  #+end_src

#+RESULTS: tree-recur-dot
#+begin_example
a [label="fib 5", shape="plaintext", fontname="monospace"];
b [label="fib 4", shape="plaintext", fontname="monospace"];
c [label="fib 3", shape="plaintext", fontname="monospace"];
d [label="fib 3", shape="plaintext", fontname="monospace"];
e [label="fib 2", shape="plaintext", fontname="monospace"];
f [label="fib 2", shape="plaintext", fontname="monospace"];
g [label="fib 1", shape="plaintext", fontname="monospace"];
h [label="fib 2", shape="plaintext", fontname="monospace"];
i [label="fib 1", shape="plaintext", fontname="monospace"];
j [label="fib 1", shape="plaintext", fontname="monospace"];
k [label="fib 0", shape="plaintext", fontname="monospace"];
l [label="fib 1", shape="plaintext", fontname="monospace"];
m [label="fib 0", shape="plaintext", fontname="monospace"];
n [label="fib 1", shape="plaintext", fontname="monospace"];
o [label="fib 0", shape="plaintext", fontname="monospace"];
p [label="1", shape="plaintext", fontname="monospace"];
q [label="1", shape="plaintext", fontname="monospace"];
r [label="0", shape="plaintext", fontname="monospace"];
s [label="1", shape="plaintext", fontname="monospace"];
t [label="0", shape="plaintext", fontname="monospace"];
u [label="1", shape="plaintext", fontname="monospace"];
v [label="0", shape="plaintext", fontname="monospace"];
a -- {b c};
b -- {d e};
c -- {f g};
d -- {h i};
e -- {j k};
f -- {l m};
g -- n;
#+end_example

#+begin_src dot :file figure0105.png :var input=tree-recur-dot :exports results
  graph {
    $input
  }
#+end_src

#+RESULTS:
[[file:figure0105.png]]

